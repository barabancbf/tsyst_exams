public class PPS {
    private LinkedList<Integer> listDigit = new LinkedList<>();
    private LinkedList<Character> listChar = new LinkedList<>();
    public String evaluate(String value) {
        return String.valueOf(make(value));
    }
    private int make(String s) {
        for (int i = 0; i < s.length(); i++) {
            char result = s.charAt(i);
            if (space(result)) {
                continue;
            }
            if (result == '(') {
                listChar.add('(');
            } else if (result == ')') {
                while (listChar.getLast() != '(') {
                    operator(listDigit, listChar.removeLast());
                }
                listChar.removeLast();
            } else if (isOperation(result)) {
                while (!listChar.isEmpty() && operatorsPriority(listChar.getLast())
                        >= operatorsPriority(result)) {
                    operator(listDigit, listChar.removeLast());
                }
                listChar.add(result);
            } else {
                StringBuilder oper = new StringBuilder();
                while (i < s.length() && Character.isDigit(s.charAt(i))) {
                    oper.append(s.charAt(i++));
                }
                --i;
                listDigit.add(Integer.parseInt(oper.toString()));
            }
        }
        while (!listChar.isEmpty()) {
            operator(listDigit, listChar.removeLast());
        }
        return listDigit.get(0);
    }
    private boolean space(char symb) {
        return symb == ' ';
    }
    private void operator(LinkedList<Integer> list, char symb) {
        int one = list.removeLast(), two = list.removeLast();
        if (symb == '+') {
            list.add(two + one);
        } else if (symb == '-') {
            list.add(two - one);
        } else if (symb == '*') {
            list.add(two * one);
        } else if (symb == '/') {
            list.add(two / one);
        } else if (symb == '%') {
            list.add(two / one);
        }
    }
    private boolean isOperation(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }
    private int operatorsPriority(char c) {
        return (c == '+' || c == '-') ? 1 : (c == '*' || c == '/' || c == '%') ? 2 : -1;
    }
}