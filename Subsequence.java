import java.util.*;
public class Subsequence {
    public static boolean find(List x, List y) {
        boolean mark = true;
        int a = 0;
            for (int i = 0; i < x.size(); i++) {
                if (!mark) {
                    break;
                }
                mark = false;
                for (int j = a; j < y.size(); j++) {
                    if (y.get(j).equals(x.get(i))) {
                        a = i + 1;
                        mark = true;
                        break;
                    }
                }
        }
        return mark;
    }
    public static void main(String[] args) {
        boolean b = find(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b);
    }
}
