import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
public class CalcTSTest {
    @Test
    public void whenGiveExpressionThenGetResult151() {
        PPS string = new PPS();
        String value = "(1+38) * 4 - 5";
        String expected = "151";
        assertThat(string.evaluate(value), is(expected));
    }
    @Test
    public void whenGiveExpressionThenGetResult100() {
        PPS string = new PPS();
        String value = "10 + (20+5) * 3 + (3 * 5)";
        String expected = "100";
        assertThat(string.evaluate(value), is(expected));
    }
}