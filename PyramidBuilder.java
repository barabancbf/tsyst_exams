import java.util.Collections;
import java.util.List;
public class PyramidBuilder {
    int floors = 0;
    boolean canBuild = false;
    int numberOfBlocks = 0, step = 1, width, height;
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if(inputNumbers.size()>100 || inputNumbers.contains(null)) {
                throw new CannotBuildPyramidException("build error");
            } else {
                Collections.sort(inputNumbers);
            }
        for (int x : inputNumbers) {
            numberOfBlocks = numberOfBlocks + step;
            floors++;
            if (numberOfBlocks == inputNumbers.size()) {
                canBuild = true;
                break;
            }
            step++;
        }
        int[][] newPyramyd;
        if (canBuild == true) {
            height = floors;
            width = floors * 2 - 1;
            int k = inputNumbers.size() - 1;
            newPyramyd = new int[height][width];
            for (int y = height - 1; y >= 0; y--) {
                int counter = (height - 1) - y;
                int nullCount = counter;
                for (int z = width - 1; z >= 0; z--) {
                    if ((z == width - 1 - counter) && (z >= nullCount)) {
                        newPyramyd[y][z] = inputNumbers.get(k);
                        k--;
                        counter = counter + 2;
                    }
                }
            }
        } else {
            throw new CannotBuildPyramidException("cannot build pyramid");
        }
        return newPyramyd;
    }
}